package lxbeego

import (
	"encoding/json"
	"gitee.com/lxgow/lxconv"
	"github.com/astaxie/beego"
	"github.com/astaxie/beego/logs"
	"strings"
)

// beego logs 封装

// Inits 初始化设置
func Inits(config map[string]string) (err error) {

	var newConfig = make(map[string]interface{})

	for k, v := range config {
		newConfig[k] = v
	}

	if config["level"] != "" {
		newConfig["level"] = lxconv.ToInt64(config["level"])
	}

	if config["maxdays"] != "" {
		newConfig["maxdays"] = lxconv.ToInt64(config["maxdays"])
	}

	if config["maxlines"] != "" {
		newConfig["maxlines"] = lxconv.ToInt64(config["maxlines"])
	}

	if config["rotate"] != "" {
		newConfig["rotate"] = lxconv.ToBool(config["rotate"])
	}

	bdata, _ := json.Marshal(newConfig)

	err = logs.SetLogger(logs.AdapterFile, string(bdata))

	if err != nil {
		return err
	}

	if strings.ToLower(config["dellogger"]) == "true" {
		_ = beego.BeeLogger.DelLogger("console")
	}

	return
}
